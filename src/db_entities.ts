import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Index, PrimaryColumn } from "typeorm";

/*
These serve two functions.
They give us types in Javascript, something that it does nto natively have.
Tehy also manage our access to teh database.
*/


// ***************************************** //
// Add the entity to teh array at the bottom //
// ***************************************** //

@Entity()
// can only have one channel set per command per server
@Index(["server", "command"], { unique: true })
export class Channels {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column("text")
    server: string;

    @Column("text")
    channel: string;

    @Column("text")
    command: string;

    @Column("text")
    user: string;

    @CreateDateColumn()
    added?: Date;

    @UpdateDateColumn()
    updated?: Date;
}

@Entity()
export class Assignments {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    server: string;

    @Column("text")
    module: string;

    @Column("text")
    title: string;

    // store4d as epoch
    @Column("integer")
    due: number;

    @Column("text")
    link: string;

    @Column("text")
    user: string;

    @Column("text", { nullable: false, default: "" })
    message_id?: string;

    @CreateDateColumn()
    added?: Date;

    @UpdateDateColumn()
    updated?: Date;
}

@Entity()
export class AssignmentsReact {
	@PrimaryGeneratedColumn()
	id: number;

	@Column("text")
	user: string;

	@Column("integer")
	assignment: number;

  @Column("boolean")
  complete: boolean;

  @Column("boolean")
  notified: boolean;

}

@Entity()
export class AssignmentsUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("text")
  server: string;

  @Column("text")
  user: string;

  @Column("text")
  module: string;
}

@Entity()
@Index(["server", "aoc_id"], { unique: true })
export class AoC {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    server: string;

    @Column("text")
    user: string;

    @Column()
    aoc_id: number;

    @UpdateDateColumn()
    updated?: Date;
}

@Entity()
@Index(["server"], { unique: true })
export class Count {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    server: string;

    @Column("text")
    channel: string;

    @Column("int")
    current_value: number;

    @Column("int")
    initial_value: number;

    @Column("int")
    base: number;

    @Column("int")
    increment: number;

    @Column("text")
    user_set_up: string;

    @Column("text")
    last_user: string;
    // Added to show who was the last person to send a number in
}

@Entity()
@Index(["server"])
export class Highscore {
    @PrimaryColumn("int")
    id: number;

    @Column("text")
    server: string;

    @Column("int")
    base: number;

    @Column("int")
    increment: number;

    @Column("text")
    user: string;

    @Column("int")
    score: number;
}

@Entity()
@Index(["server"])
export class Bap {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column("text")
  server: string;

  @Column("text")
  user: string;

  @Column("boolean")
  bap: boolean;

  @Column("int")
  bap_current: number;

  @Column("int")
  bap_max: number;

  @Column("int")
  bap_total: number;
}

export const entities = [
    Channels,
    Assignments,
		AssignmentsReact,
  AssignmentsUser,
    AoC,
    Count,
    Highscore,
  Bap,
];